#!/usr/bin/env python
# -*- coding:utf-8 -*-
# author : liuyu
# date  2019/3/13

from django.forms import ModelForm
from django import forms
from crm import models

class CustomerForm(ModelForm):
    class Meta:
        model=models.CustomerInfo
        # fields=["name","consultant","status"]
        fields = "__all__"
        exclude=["consult_content","status","consult_courses"]
        readonly_fields =["contact_type","contact","consultant","referral_from","source"]

    def clean(self):
        print("cleanned_data",self.cleaned_data)

        if self.errors:
            raise  forms.ValidationError("please fix errors before re-submit")

        if self.instance.id is not None:
            for field in self.Meta.readonly_fields:
                old_field_val = getattr(self.instance,field)
                form_val = self.cleaned_data.get(field)
                print("filed differ compare:",old_field_val,form_val)
                if old_field_val !=form_val:
                    self.add_error(field,"Readonly fileds: field should be '{value}' ,not '{new_value}'"
                                   .format(**{'value':old_field_val,'new_value':form_val}))


    def __new__(cls,*args,**kwargs):
        for field_name in cls.base_fields:
            filed_obj = cls.base_fields[field_name]
            filed_obj.widget.attrs.update({'class':'form-control'})

            if field_name in cls.Meta.readonly_fields:
                filed_obj.widget.attrs.update({'disabled': 'true'})

        return ModelForm.__new__(cls)