from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import  login_required
from  crm import models
from crm import froms
# Create your views here.

@login_required
def dashboard(request):

    return render(request, 'crm/dashboard.html')



@login_required
def stu_enrllment(request):

    customers = models.CustomerInfo.objects.all()
    class_list = models.ClassList.objects.all()


    print(request.user.userprofile.id)

    if request.method == "POST":
        print(request.POST)
        customer_id = request.POST.get("customer")
        class_id = request.POST.get("class")

        enrollment_obj=models.StudentEnrollment.objects.create(customer_id=customer_id,class_grade_id=class_id,
                                                consultant_id=request.user.userprofile.id)

        enrollment_url = "http://localhost:8000/crm/enrollment/%s" %enrollment_obj.id


    return render(request,"crm/stu_enrollment.html",locals())


def enrollment(request,enrollment_id):
    '''学员在线报名地址'''

    enrollment_obj = models.StudentEnrollment.objects.get(id=enrollment_id)

    if request.method == "POST":
        print(request.POST)
        customer_form = froms.CustomerForm(instance=enrollment_obj.customer,data=request.POST)
        if customer_form.is_valid():
            customer_form.save()
            return  HttpResponse("您已经成功提交报名信息")
        print("form error",customer_form.errors)
    else:
        customer_form = froms.CustomerForm(instance=enrollment_obj.customer)




    return render(request,'crm/enrollment.html',locals())