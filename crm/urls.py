
from django.conf.urls import url,include
from crm import views

urlpatterns = [

    url(r'^$', views.dashboard,name="sales_dashboard"),
    url(r"^stu_enrllment$",views.stu_enrllment,name="stu_enrllment"),
    url(r"^enrollment/(\w+)",views.enrollment,name="enrollment"),

]
