from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django import conf
import json
from kingadmin import form_handel

from kingadmin import app_setup
from django.core.paginator import Paginator
from crm import models
app_setup.kingadmin_auto_discover()


from kingadmin.sites import  site
print("sites.",site.enabled_admins)

# for k,v in site.enabled_admins.items():
#     for table_name,admin_class in v.items():
#         print(table_name,id(admin_class))
# # Create your views here.


def app_index(request):
    #enabled_admins =

    return render(request,'kingadmin/app_index.html', {'site':site})

def get_filter_result(request,querysets):
    filter_conditions = {}
    for key,val in request.GET.items():
        if key in ('_page','_o','_q'):
            continue
        if val:
            filter_conditions[key] =  val


    print("filter_conditions",filter_conditions)
    return querysets.filter(**filter_conditions),filter_conditions

def get_order_by_result(request,querysets,admin_class):
    oid = request.GET.get("_o")
    current_column={}
    if oid:
        okey=admin_class.list_display[abs(int(oid))]
        current_column[okey]=oid
        if oid.startswith("-"):
            okey="-%s" %okey
        return querysets.order_by(okey),current_column
    else:
        return querysets,current_column

from django.db.models import Q

def get_searchd_result(request,querysets,admin_class):
    q =Q()
    q.connector = 'OR'

    search_key = request.GET.get("_q")
    if search_key:
        for search_filed in admin_class.search_fields:
            q.children.append(("%s__contains" %search_filed,"%s" %search_key))

        return querysets.filter(q)

    return querysets

@login_required
def table_obj_add(request,app_name,model_name):
    admin_class = site.enabled_admins[app_name][model_name]
    model_form = form_handel.create_dynamic_model_form(admin_class,form_add=True)
    if request.method == "GET":
        form_obj = model_form()
    elif request.method == "POST":
        form_obj = model_form(data=request.POST)
        if form_obj.is_valid():
            type=form_obj.save()
            print("保存成功",type)
            return redirect('/kingadmin/%s/%s/' % (app_name, model_name))
        else:
            print("验证失败")
    return render(request,"kingadmin/table_obj_add.html",locals())


@login_required
def table_obj_delete(request,app_name,model_name,obj_id):
    admin_class = site.enabled_admins[app_name][model_name]
    form_obj = admin_class.model.objects.get(id=obj_id)

    if request.method == "POST":
        print(request.POST)
        a=form_obj.delete()
        return redirect('/kingadmin/%s/%s/' % (app_name, model_name))
    return render(request,'kingadmin/table_obj_delete.html',locals())


@login_required
def table_obj_change(request,app_name,model_name,obj_id):
    admin_class = site.enabled_admins[app_name][model_name]
    model_form = form_handel.create_dynamic_model_form(admin_class,form_add=False)
    obj = admin_class.model.objects.get(id=obj_id)

    if request.method == "GET":

        form_obj = model_form(instance=obj)
    elif request.method == "POST":
        print(request.POST)
        form_obj = model_form(instance=obj,data=request.POST)
        if form_obj.is_valid():
            form_obj.save()
            return redirect('/kingadmin/%s/%s/' %(app_name,model_name))

    return render(request,"kingadmin/table_obj_change.html",locals())

@login_required
def table_obj_list(request,app_name,model_name):
    """取出指定model里的数据返回给前端"""
    #print("app_name,model_name:",site.enabled_admins[app_name][model_name])
    admin_class = site.enabled_admins[app_name][model_name]

    if request.method == "POST":
        selected_action = request.POST.get("action")
        selected_ids = json.loads(request.POST.get("selected_ids"))
        select_objs = admin_class.model.objects.filter(id__in=selected_ids)
        if not selected_action:
            select_objs.delete()
        else:
            admin_action_func = getattr(admin_class,selected_action)
            return admin_action_func(request,select_objs)


    querysets = admin_class.model.objects.all().order_by('id')

    querysets,filter_condtions  = get_filter_result(request,querysets)
    admin_class.filter_condtions = filter_condtions

    ## 搜索
    querysets = get_searchd_result(request,querysets,admin_class)


    ##排序##

    querysets ,current_column= get_order_by_result(request,querysets,admin_class)

    ##分页##
    paginator = Paginator(querysets,admin_class.list_per_page)
    page = request.GET.get("_page")
    querysets = paginator.get_page(page)



    print(request.GET)
    #print("admin class",admin_class.model )
    print(current_column)
    search_key = request.GET.get("_q",'')
    return render(request,'kingadmin/table_obj_list.html', locals())


def acc_login(request):
    error_msg = ''
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username,password=password)
        if user:
            print("passed authencation",user)
            login(request,user)
            #request.user = user

            return  redirect( request.GET.get('next','/kingadmin/') )
        else:
            error_msg = "Wrong username or password!"
    return render(request, 'kingadmin/login.html', {'error_msg':error_msg})


def acc_logout(request):
    logout(request)
    return redirect("/login/")
