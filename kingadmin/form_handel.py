#!/usr/bin/env python
# -*- coding:utf-8 -*-
# author : liuyu
# date  2019/3/13
from django.forms import ModelForm

def create_dynamic_model_form(admin_class,form_add=False):
    admin_class.form_add=form_add
    '''动态生成 model form ,默认为修改的表单'''
    class Meta:
        model = admin_class.model
        if not form_add:
            exclude =admin_class.readonly_fields
        fields = "__all__"

    def __new__(cls,*args,**kwargs):
        red_fields = admin_class.readonly_fields
        if form_add:
            admin_class.readonly_fields = []
        for field_name in cls.base_fields:
            filed_obj = cls.base_fields[field_name]
            filed_obj.widget.attrs.update({'class':'form-control'})
            if field_name in admin_class.readonly_fields:
                    filed_obj.widget.attrs.update({'disabled': 'true'})

        print(cls.base_fields)
        admin_class.readonly_fields = red_fields
        return ModelForm.__new__(cls)

    dynamic_form = type("DynamicModelForm",(ModelForm,),{'Meta':Meta,"__new__":__new__})
    return dynamic_form