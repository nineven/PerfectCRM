from django.template import Library
from django.utils.safestring import mark_safe
import datetime ,time
register = Library()

@register.simple_tag
def build_filter_ele(filter_column,admin_class):

    column_obj = admin_class.model._meta.get_field(filter_column)
    print("column obj:",column_obj)
    try:
        filter_ele = "<select name='%s' >" % filter_column
        for choice in column_obj.get_choices():
            selected = ''
            if filter_column in admin_class.filter_condtions:#当前字段被过滤了
                print("filter_column", choice,)
                #       type(admin_class.filter_condtions.get(filter_column)),
                #       admin_class.filter_condtions.get(filter_column))
                if str(choice[0]) == admin_class.filter_condtions.get(filter_column):#当前值被选中了
                    selected = 'selected'
                    print('selected......')

            option = "<option value='%s' %s>%s</option>" % (choice[0],selected,choice[1])
            filter_ele += option
    except AttributeError as e:
        print("err",e)
        filter_ele = "<select name='%s__gte'>" % filter_column
        if column_obj.get_internal_type() in ('DateField','DateTimeField'):
            time_obj = datetime.datetime.now()
            time_list = [
                ['','------'],
                [time_obj,'Today'],
                [time_obj - datetime.timedelta(7),'七天内'],
                [time_obj.replace(day=1),'本月'],
                [time_obj - datetime.timedelta(90),'三个月内'],
                [time_obj.replace(month=1,day=1),'YearToDay(YTD)'],
                ['','ALL'],
            ]

            for i in time_list:
                selected = ''
                time_to_str = ''if not i[0] else  "%s-%s-%s"%(i[0].year,i[0].month,i[0].day)
                if  "%s__gte"% filter_column in admin_class.filter_condtions:  # 当前字段被过滤了
                    print('-------------gte')
                    if time_to_str == admin_class.filter_condtions.get("%s__gte"% filter_column):  # 当前值被选中了
                        selected = 'selected'
                option = "<option value='%s' %s>%s</option>" % \
                         (time_to_str ,selected,i[1])
                filter_ele += option

    filter_ele += "</select>"
    return mark_safe(filter_ele)




@register.simple_tag
def  build_table_row(obj,admin_class):
    """生成一条记录的html element"""

    ele = ""
    if admin_class.list_display:
        for index,column_name in enumerate(admin_class.list_display):


            column_obj = admin_class.model._meta.get_field(column_name)
            if column_obj.choices: #get_xxx_display
                column_data = getattr(obj,'get_%s_display'% column_name)()
            else:
                column_data = getattr(obj,column_name)
            if index==0:
                td_ele = "<td><a href='%s/change'>%s</a></td>"% (obj.id,column_data)
            else:
                td_ele = "<td>%s</td>"% column_data

            ele += td_ele
    else:
        td_ele = "<td><a href='%s/change'>%s</a></td>" %(obj.id,obj)
        ele += td_ele
    return mark_safe(ele)


@register.simple_tag
def model_ad(admin_class):
    return admin_class.model._meta.model_name.upper()

@register.simple_tag
def page_content(querysets):

    ele=""
    for i in querysets.paginator.page_range:
        if abs(querysets.number-i)<=2:
            if querysets.number == i:
                ele+='<li class="page-item active"><a class="page-link" href="?_page=%s">%s</a></li>' %(i,i)
            else:
                ele+='<li class="page-item"><a class="page-link" href="?_page=%s">%s</a></li>' %(i,i)

    return mark_safe(ele)

@register.simple_tag
def get_sort_columen(column ,current_column ,forloop,admin_class):
    cval=forloop
    filter_condtions=admin_class.filter_condtions
    elea = ''

    if filter_condtions:
        for k,v in filter_condtions.items():
            elea+="&%s=%s" %(k,v)
    if column in current_column:
        cval = current_column.get(column)
        if cval.startswith("-"):
            cval=str(cval.split("-")[1])
            info="fas fa-sort-down"

        else:
            cval="-%s" %cval
            info="fas fa-sort-up"
        ele = ' <th ><a href="?_o=%s" >%s</a> <i class="%s"></i></th>' % (cval+elea, column,info)
    else:
        ele=' <th ><a href="?_o=%s" >%s</a></th>' %(str(cval)+elea,column)
    return mark_safe(ele)


@register.simple_tag
def get_obj_field_val(model_form,fields):
    '''返回 model 具体字段的值'''

    return getattr(model_form.instance,fields)

@register.simple_tag
def get_availble_m2m_data(field_name,admin_class,form_obj):
    field_obj = admin_class.model._meta.get_field(field_name)
    obj_list = set(field_obj.related_model.objects.all())
    try:
        selected_data=set(getattr(form_obj.instance,field_name).all())
    except:
        selected_data=set([])
    return obj_list-selected_data

@register.simple_tag
def get_selected_m2m_data(field_name,form_obj):

    try:
        selected_data=getattr(form_obj.instance,field_name)
        obj_list = selected_data.all()
    except:
        obj_list=[]
    return obj_list

@register.simple_tag
def display_all_related_objs(obj):
    '''遍历 显示要被删除对象的所有'''

    ele = "<ul>"
    ele += "<li>%s</li>" %obj


    for reversed_fk_obj in obj._meta.related_objects:

        # print(obj._meta.model_name)

        related_table_name = reversed_fk_obj.name
        # print(related_table_name)
        related_lookup_key = "%s_set" %related_table_name
        related_objs = getattr(obj,related_lookup_key).all() #反向查询所有关联的数据

        ele += "<li> %s </li><ul>" %related_table_name

        if reversed_fk_obj.get_internal_type == "ManyToManyField":
            for i in related_objs:
                ele += "<li> %s  记录将要删除</li>" % i
        else:
            for i in related_objs:
                ele += "<li> %s </li>" %i
                if obj._meta.model_name != reversed_fk_obj.name:
                    ele+=display_all_related_objs(i)
        ele+="</ul></li>"


    ele+="</ul>"

    return mark_safe(ele)


@register.simple_tag
def get_model_verbose_name(admin_class):
    return admin_class.model._meta.verbose_name