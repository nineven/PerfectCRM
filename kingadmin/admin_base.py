from django.shortcuts import render,redirect
import json


class BaseKingAdmin(object):
    def __init__(self):
        if not set(self.default_actions).intersection(self.actions) :
         self.actions.extend(self.default_actions)


        print("admin---------------",self.actions)
    list_display =[]
    list_filter = []
    search_fields = []
    readonly_fields =[]
    list_per_page = 10
    actions = []
    default_actions=["delete_selected_obj"]
    def delete_selected_obj(self,request,querysets):

        querysets_ids=json.dumps([i.id for i in querysets])
        return render(request,'kingadmin/table_obj_delete.html',{"admin_class":self,"querysets":querysets,"querysets_ids":querysets_ids})
